let text = "Hello"; //string
let number = 15; //number
let trueorfalse = true; //boolean
let empty; //undefined
let object = {firstName: "Jan", secondName: "Kowalski", age: 36}; //object
let sym = Symbol(); //symbol
let add = text + number;
console.log(typeof text);
console.log(typeof number);
console.log(typeof trueorfalse);
console.log(typeof empty);
console.log(typeof object);
console.log(typeof sym);
console.log(typeof add); // string + number = string