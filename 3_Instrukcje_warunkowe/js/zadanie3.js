function myEval(a, b, operation) {
    var result = 0;
    switch(operation) {
        case '+': 
            result = a + b;
            return result;
        case '-': 
            result = a - b;
            return result;
        case '/': 
            result = a / b;
            return result;
        case '*': 
            result = a * b;
            return result;
        case '%': 
            result = a % b;
            return result;
        case '^': 
            result = Math.pow(a, b);
            return result;
    }
}
console.log(` 2 + 4 = ${myEval(2, 4, "+")}`);
console.log(` 5 - 2 = ${myEval(5 - 2, "-")}`);
console.log(` 4 ^ 8 = ${myEval(4 , 8, "^")}`);
console.log(` 10 - 2 = ${myEval(10, 2, "-")}`);
console.log(` 8 * 5 = ${myEval(8 , 5, "*")}`);
console.log(` 9 % 2 = ${myEval(9, 2, "%")}`);
